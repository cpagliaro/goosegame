package go.ose.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Player {
	private final HashMap<String, Integer> positions = new HashMap<String, Integer>();
	private int startPosition = 0;

	public void addPlayer(String playerName) {
		if (!positions.containsKey(playerName.toLowerCase())) {
			// la casella Start iniza a 0, come da documentazione.
			positions.put(playerName.toLowerCase(), startPosition);
			String wMsg = "";
			if (positions.size() == 1) {
				wMsg = "players: " + playerName;
			} else {
				wMsg = "players: " + positions.keySet().toString();
			}
			System.out.println(wMsg);
		} else {
			System.out.println(playerName + ": already existing player");
		}
	}

	public void movePlayer(String playerName, int diceOne, int diceTwo) {
		StringBuilder builder = new StringBuilder(playerName + " rolls " + diceOne + ", " + diceTwo + ". ");

		if (positions.containsKey(playerName.toLowerCase())) {
			int currentPosition = positions.get(playerName.toLowerCase()).intValue();
			int diceRollResult = diceOne + diceTwo;
			int newPosition = checkPosition(builder, playerName, currentPosition, diceRollResult);
			positions.put(playerName.toLowerCase(), newPosition);
			/**
			 * As a player, when I land on a space occupied by another player, I send him to
			 * my previous position so that the game can be more entertaining.
			 */
			prank(builder, playerName, currentPosition, newPosition);
			if (newPosition == 63) {
				Board.setWinner(playerName);
			}
			System.out.println(builder.toString());
		} else {
			System.out.println("Unexisting player!");
		}
	}

	private int checkPosition(StringBuilder builder, String playerName, int currentPosition, int diceRollResult) {
		/**
		 * Get to "The Bridge"
		 */
		if ((currentPosition + diceRollResult) == 6) {
			builder.append(
					playerName + " moves from " + currentPosition + " to The Bridge. " + playerName + " jumps to 12");
			return 12;
		}
		return gooseSteps(builder, playerName, currentPosition, diceRollResult);
	}

	private int gooseSteps(StringBuilder builder, String playerName, int currentPosition, int diceRollResult) {

		List<Integer> gooses = Arrays.asList(5, 9, 14, 18, 23, 27);

		List<Integer> gooseSteps = new ArrayList<Integer>(6);

		int newPosition = currentPosition + diceRollResult;
		/**
		 * Multiple Jump
		 */
		while (gooses.contains(newPosition)) {
			gooseSteps.add(newPosition);
			newPosition += diceRollResult;
		}

		if (!gooseSteps.isEmpty()) {
			/**
			 * Single Jump
			 */
			builder.append(
					playerName + " moves from " + currentPosition + " to " + gooseSteps.get(0) + ", The Goose. ");
			/**
			 * Multiple Jump
			 */
			if (gooseSteps.size() > 1) {
				for (int i = 1; i < gooseSteps.size(); i++) {
					builder.append(playerName + " moves again and goes to " + gooseSteps.get(i) + ", The Goose. ");
				}
			}
			int lastPosition = gooseSteps.get(gooseSteps.size() - 1).intValue() + diceRollResult;
			builder.append(playerName + " moves again and goes to " + lastPosition + ".");
			return lastPosition;
		}
		newPosition = checkForBounce(builder, playerName, currentPosition, currentPosition + diceRollResult);
		return newPosition;
	}

	private int checkForBounce(StringBuilder builder, String playerName, int oldPosition, int newPosition) {
		if (newPosition > 63) {
			int backPosition = 2 * 63 - newPosition;
			builder.append(playerName + " moves from " + oldPosition + " to 63. " + playerName + " bounces! "
					+ playerName + " returns to " + backPosition + ".");
			return backPosition;
		}
		builder.append(
				playerName + " moves from " + (oldPosition == 1 ? "Start" : oldPosition) + " to " + newPosition + ".");
		return newPosition;
	}

	private void prank(StringBuilder builder, String playerName, int position, int newPosition) {
		for (Map.Entry<String, Integer> entry : positions.entrySet()) {
			if (!entry.getKey().equals(playerName.toLowerCase()) && entry.getValue().intValue() == newPosition) {
				entry.setValue(position);
				builder.append(
						" On " + newPosition + " there is " + entry.getKey() + ", who returns on " + position + ".");
				break;
			}
		}

	}

	public void movePlayer(String playerName) {
		movePlayer(playerName, diceRoll(), diceRoll());
	}

	private int diceRoll() {
		return new Random().nextInt(6) + 1;
	}

}
