package go.ose.game;

import java.util.Scanner;

public class Board {

	private static Scanner in = new Scanner(System.in);

	private Player players;
	private static String winner;

	public void start() {
		players = new Player();
		guide();
		do {
			action(in.nextLine());
		} while (winner == null);
		System.out.println(winner + " Wins!!");
	}

	public static void setWinner(String playerName) {
		winner = playerName;
	}

	private void action(String command) {
		String[] commands = command.split(" ");
		switch (commands[0]) {
		case "move":
			if (commands.length >= 2 || commands.length <= 4) {
				movePlayer(commands);
				break;
			}
		case "add":
			if (commands.length == 3 && (commands[1].equals("player"))) {
				players.addPlayer(commands[2]);
				break;
			}
		default:
			System.out.println("Unexisting command!");
			break;
		case "exit":
			System.out.println("Exit Game Success!");
			System.exit(0);
		case "help":
			guide();
		}
	}

	private void guide() {
		System.out.println("Welcome to the Goose Game! \n To add a player type \"add player Pippo\".\n"
				+ "To move a player type \"move Pippo\" or \"move Pippo 4, 2\".\n" + "digit \"help\" for this guide.\n"
				+ "digit \"exit\" to exit the game.");
	}

	private void movePlayer(String[] args) {
		switch (args.length) {
		case 2:
			players.movePlayer(args[1]);
			break;
		case 3:
			parseSteps(args[1], args[2]);
			break;
		case 4:
			parseSteps(args[1], args[2] + args[3]);
			break;
		default:
			System.out.println("Something went wrong!");
			break;
		}
	}

	private void parseSteps(String playerName, String steps) {
		if (steps.length() == 3 && steps.charAt(1) == ',') {
			int diceOne = Character.getNumericValue(steps.charAt(0));
			int diceTwo = Character.getNumericValue(steps.charAt(2));

			if (diceOne > 0 && diceOne < 7 && diceTwo > 0 && diceTwo < 7) {
				players.movePlayer(playerName, diceOne, diceTwo);
			} else {
				System.out.println("Dices have number between 0 and 6!");
			}
		} else {
			System.out.println("Unexisting command!");
		}
	}
}
